<?php 
include './scripts/server/connect_db.php';

$blogs = "SELECT * from blogs LIMIT 4";
$blogs_result = mysqli_query($connect,$blogs);

$users = "SELECT * from users LIMIT 4";
$users_result = mysqli_query($connect,$users);

$category = "SELECT * from category";
$category_result = mysqli_query($connect,$category);
?>
<footer>
    <div class="d-flex justify-evenly FooterContent">
        <div class="FooterItem">
            <img src="styles/images/Hashtag-Blog-Logo.png" width="100">
            <h2 class="font-size1 my-3">هشتگ بلاگ</h2>
            <p class="font-size-8 text-secondary">شبکه ای از بلاگ ها و بلاگر ها</p>
        </div>

        <div class="FooterItem ">
            <h2>آخرین بلاگ ها</h2>
            <?php while($row = mysqli_fetch_assoc($blogs_result)){?>
                <div class="FooterLinks">
                    <a href="posts?blogId=<?php echo $row['id']?>" 
                        class="text-secondary font-size-8"
                        target="_blank">
                        <?php echo $row['name']?>
                    </a>
                </div>
            <?php }?>
            <div class="moreLink"><a href="blogs">همه بلاگ ها</a></div>
        </div>

        <div class="FooterItem">
            <h2>آخرین بلاگر ها</h2>
            <?php while($row = mysqli_fetch_assoc($users_result)){?>
                <div class="FooterLinks">
                    <a href="user?id=<?php echo $row['id']?>&user=<?php echo $row['user_name']?>" 
                        target="_blank"
                        class="text-secondary font-size-8">
                        <?php echo $row['name'] ?>
                    </a>
                </div>
            <?php } ?>
            <div class="moreLink"><a href="blogers">همه بلاگر ها</a></div>
        </div>

        <div class="FooterItem">
            <h2>موضوعات</h2>
            <?php while($row = mysqli_fetch_assoc($category_result)){?>
                <div class="FooterLinks">
                    <a href="#" class="text-secondary font-size-8">
                        <?php echo $row['title']?>
                    </a>
                </div>
            <?php } ?>
        </div>
        
        <div class="FooterItem">
            <h2>درباره ما</h2>
            <div class="FooterLinks"><a href="#">هشتگ بلاگ چیست ؟</a></div>
            <div class="FooterLinks"><a href="#">چرا هشتگ بلاگ ؟</a></div>
        </div>
    </div>

    <div class="Contact d-flex justify-center py-2">
        <span>راه های ارتباطی : </span>
        <a href="#"><img src="styles/images/telegram.svg" width="15" alt="telegram"></a>
        <a href="#"><img src="styles/images/instagram.svg" width="14" alt="instagram"></a>
        <a href="#"><img src="styles/images/close-envelope.svg" width="15" alt="email"></a>
    </div>

    <div class="CopyRight">
        <span>
        ساخته شده توسط
        <a href="http://mehdi-khoshnevisz.ir" target="_blank" class="font-size-7 text-secondary trans400 align-middle font-weight-bold">Mehdi Khoshnevisz</a>
        </span>
    </div>
</footer>
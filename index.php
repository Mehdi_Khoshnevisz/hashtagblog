<?php

include 'scripts/server/routes.php';

function getCurrentUri(){
    $basepath = implode('/', array_slice(explode('/', $_SERVER['SCRIPT_NAME']), 0, -1)) . '/';
    $uri = substr($_SERVER['REQUEST_URI'], strlen($basepath));
    if (strstr($uri, '?')) $uri = substr($uri, 0, strpos($uri, '?'));
    $uri = '/' . trim($uri, '/');
    return $uri;
}
$base_url = getCurrentUri();
$request = $_SERVER['REQUEST_URI'];
$router  = new Router($base_url);
// routes
$router->get('/','./pages/home');
$router->get('/blogs','./pages/blogs');
$router->get('/post','./pages/post');
$router->get('/posts','./pages/posts');
$router->get('/blogers','./pages/blogers');
$router->get('/user','./pages/user-profile');
$router->get('/create','./pages/blog-create');
$router->get('/write','./pages/blog-write');
$router->get('/signin','./pages/signin');
$router->get('/signup','./pages/signup');
$router->get('/logout','./pages/logout');
$router->get('/successful','./pages/successful');
$router->get('/admin','./pages/SiteAdmin/index'); //admin route

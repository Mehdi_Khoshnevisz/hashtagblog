$('.search-icon').click(function(){
    $('.search-container').addClass('search-container-show');
});

$('.search-container').click(function(e){
    let className = e.target.getAttribute('class');
    if (className === 'search-input') e.preventDefault();
    else $('.search-container').removeClass('search-container-show');
})

$(document).ready(function() {

    // Variables
    var step = 0;
    var step_length = $('.step-item').length;

    // Show first item
    $('.step-item').eq(step).addClass('this-step-show');

    // Next step
    $('.next-step').click(function() {
        step = $(this).parents('.step-item').index();
        $('.step-item').removeClass('this-step-show');
        $('.step-item').eq(step + 1).addClass('this-step-show');
        
    });
    
    // When steps was end
    function StepWillEnd(element,text){
        ChangeText(element,text);
    }
    
});
var gulp = require('gulp'),
    sass = require('gulp-sass');

gulp.task('styles',function() {
    gulp.src('styles/sass/base.scss')
        .pipe(sass().on('error',sass.logError))
        .pipe(gulp.dest('styles/css/'))
});

//watch tasks
// gulp.task('default',function() {
//    gulp.watch('styles/sass/**/*.scss',['styles','webserver']); 
// });

gulp.task('default',['styles']);
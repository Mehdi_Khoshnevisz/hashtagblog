<?php 
session_start();
include './scripts/server/connect_db.php';
$blogs = "SELECT * from blogs LIMIT 4";
$blogs_result = mysqli_query($connect,$blogs);

$users = "SELECT * from users LIMIT 4";
$users_result = mysqli_query($connect,$users);

$category = "SELECT * from category";
$category_result = mysqli_query($connect,$category);
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <title>هشتگ بلاگ |‌ شبکه‌ای از بلاگ‌ها و بلاگرها</title>
    <link rel="shortcut icon" href="./styles/images/Hashtag-Blog-Logo.ico" type="image/x-icon">
    <link rel="stylesheet" type="text/css" media="screen" href="./node_modules/bootstrap/dist/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="./styles/css/base.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="./styles/css/main.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="./styles/css/header.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="./styles/css/search.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="./styles/css/home.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="./styles/css/footer.css" />
</head>

<body>

    <?php include './partials/header.php' ?>

    

    <main>

        <?php include './partials/search.php' ?>

        <section class="Background">
            <div class="container d-flex justify-content-evenly">
                <div class="m-auto">
                    <div class="BoxItem text-right">
                        <h1 class="font-size1-7 font-weight-bold text-right">هشتگ بلاگ ; شبکه‌ای از بلاگ ها و بلاگرها</h1>
                        <p class="font-size1 mt-3">
                            در هشتگ بلاگ مطالب تخصصی خودت رو منتشر کن و به اشتراک بذار ، صفحات رو دنبال کن ، مطالب  دوستانت رو لایک کن و پرسش و پاسخ انجام بده ...
                        </p>
                    </div>
                </div>
                <div class="d-flex">
                    <img src="styles/images/Hashtag-Blog-Logo.png" width="450" style="margin: auto 0;" alt="">
                </div>
            </div>
        </section>


        <section>
            <nav class="Category  flex-wrap bg-light">
                <ul class="m-0 p-0 d-flex justify-content-center text-center">
                <?php  while($row = mysqli_fetch_assoc($category_result)){ ?>
                    <li class="">
                        <a href="#">
                            <?php echo $row['title']?>
                        </a>
                    </li>
                <?php }?>
                </ul>
            </nav>
        </section>



        <section class="Info d-flex justify-evenly align-items-center">
            <div class="BlogTitle">
                <h2 class=" text-center" style="color:white;">دوست داری در مورد چی بدونی ؟</h2>
                <form action="">
                    <input type="text" name="search" placeholder="در بین بلاگ ها جستجو کن ...">
                </form>
            </div>
        </section>



        <section class="container py-2">
            <div class="d-flex justify-content-between align-items-center px-2">
                <h3 class="text-right my-3 font-size1-3 font-weight-bold">آخرین بلاگ‌ها</h3>
                <a href="blogs" class="font-size1 text-blue">دیدن همه بلاگ ها</a>
            </div>
            <div class="d-flex flex-wrap">
                <?php  while($row = mysqli_fetch_assoc($blogs_result)){ ?>
                    <div class="col-md-3 px-2 my-2">    
                        <div class="card blogItem bg-light text-right p-2">
                            <figure class="m-0">
                                <a href="posts?blogId=<?php echo $row['id']?>">
                                    <img class="w-100 rounded" src="styles/images/blog_img.jpg" >
                                </a>
                            </figure>
                        </div>
                    </div>
                <?php }?>
            </div>
        </section>


        <section class="container py-2 ">
            <div class="d-flex justify-content-between align-items-center px-2">
                <h3 class="text-right my-3 font-size1-3 font-weight-bold">جدیدترین بلاگرها</h3>
                <a href="blogers" class="font-size1 text-blue">دیدن همه بلاگر ها</a>
            </div>
            <div class="d-flex flex-wrap">
                <?php while($row = mysqli_fetch_assoc($users_result)){ ?>
                    <div class="col-md-3 px-2 my-2">    
                        <div class="blogItem text-center p-2">
                            <figure class="m-0">
                                <a href="#">
                                    <img class="rounded-circle mx-auto" width="150" height="150" src="styles/images/blog_img.jpg" style="object-fit:contain" >
                                    <h3 class="font-size1"><?php echo $row['name']?></h3>
                                </a>
                            </figure>
                        </div>
                    </div>
                <?php }?>
            </div>
        </section>


        <section class="d-flex bg-primary justify-content-center align-items-center" style="padding:50px 0;">
            <div>
                <a class="font-size-9 text-white font-weight-bold" href="./signup">همین الان به جمع بلاگرها بپیوند...</a>
            </div>
        </section>


        <section class="d-flex justify-evenly align-items-center">
            <div class="BlogTitle text-center">
                <h2 class="text-center text-secondary">در بخش
                    <a class="question_link" href="#">پرسش و پاسخ</a> 
                    سوال خودتو مطرح کن
                </h2>
                <a href="#" class="d-block mt-3">
                    <button class="btn btn-primary font-size1-2 border-0 px-5 py-1">بپرس</button>
                </a>
            </div>
            <div class="text-right" >
                <img src="styles/images/question.png" alt="">
            </div>
        </section>
    </main>

    <?php include './partials/footer.php' ?>

    <!-- scripts -->
    <script src="node_modules/jquery/dist/jquery.min.js"></script>
    <script src="scripts/client/public.js"></script>
    <script src="scripts/client/search.js"></script>

</body>
</html>


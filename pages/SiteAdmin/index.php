<?php
include_once('view.php');
include_once('model.php');
include_once('controller.php');

?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" media="screen" href="./styles/css/admin.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="./styles/css/header.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="./styles/css/base.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="./styles/css/main.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="./styles/css/header.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="./styles/css/search.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="./styles/css/home.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="./styles/css/footer.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="./node_modules/bootstrap/dist/css/bootstrap.min.css" />
    <title>Administrator</title>
  </head>
  <body>
    <?php echo authenticationUser(); ?>
  </body>
</html>

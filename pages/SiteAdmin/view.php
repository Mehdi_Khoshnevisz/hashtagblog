
<?php
include_once('admin_panel.php');
function viewLogin(){
  ?>
  <form action="" method="post" name="signin_form" class="d-flex flex-column justify-content-center align-items-center" style="height:100vh">
      <div class="text-center flex-1">
          <div class="d-flex justify-content-between align-items-center py-3">
              <h3 class="text-secondary m-0">مدیریت </h3>
              <a href="/hashtagblog"><img src="styles/images/Hashtag-Blog-Logo.png" width="80"></a>
          </div>
          <div class="card box-shadow bg-light">
              <div class="form-group px-3 m-0">
                  <div class="my-3">
                    <input class="form-control" type="text" name="username" placeholder="نام کاربری"/>
                  </div>
                  <div class="my-3">
                      <input class="form-control" type="password" name="password" placeholder="رمز عبور" />
                  </div>
                  <div class="my-3">
                      <button class="btn btn-sm w-100 btn-success rounded">ورود</button>
                  </div>
              </div>
          </div>
      </div>
  </form>
  <?php
}
function viewError(){
  echo "error is accured";
}
?>

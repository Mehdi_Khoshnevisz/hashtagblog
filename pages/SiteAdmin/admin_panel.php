<?php
function viewPanel(){
  $data = getPanelInfo();
  // print_r($data);
?>
<header>

        <div class="container d-flex justify-content-between align-items-center">
            <nav>
                <ul>
                    <li >
                      <button class="navbar-toggler" onclick="openNav()" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon">menu</span>
                      </button>
                    </li>
                    <li>
                      <a href="#">
                        <img src="styles/images/Hashtag-Blog-Logo.png" width="50" style="vertical-align:middle" alt="">
                      </a>
                    </li>
                </ul>
            </nav>
            <div class="Login">
                <?php if(isset($_SESSION['user'])){ ?>
                    <a href="#" class="text-secondary">
                        <?php echo $_SESSION['user'] ?>
                    </a>
                    <a href="logout">خروج</a>
                <?php } ?>

            </div>
        </div>

</header>



<div id="mySidenav" class="sidenav">
 <span href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</span>
 <a href="#">مدیریت کاربران</a>
 <a href="#">مدیریت مقالات</a>
 <a href="#">آمار و ارقام</a>
 <a href="#">گزارش ها</a>
</div>
<br>
<div class="container">
  <div class="row">
    <div class="col">
      <div class="card">
        <div class="card-header">
          مدیریت سایت
        </div>
        <!-- <div class="card-body">
          <h6 class="card-subtitle mb-2 text-muted">ویرایش - حذف و مدیریت اعضا </h6>
          <p class="card-text">مدیریت اعضا و پست ها و گزارشات سایت به همراه امار و ارغام سایت</p>
          <a href="#" class="card-link">Card link</a>
          <a href="#" class="card-link">Another link</a>
        </div> -->
      </div>
    </div>
  </div>
  <hr style="border-color:none;border-top:1px solid #eee;"/>
  <div class="row">
    <div class="col">
      <div class="card">
        <div class="card-header">
        مشاهده آمار سایت بصورت چکیده
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col">
              <div class="card">
                <div class="card-header">
                  آخرین پست ها
                </div>
                <div class="card-body">
                  <!-- //later we add a table here -->
                  <?php for ($i=0; $i < 4; $i++) {
                    echo "<span class='badge badge-secondary'>".($data[2][$i]['title'])."</span>";
                    echo " ";
                  }
                  ?>

                </div>
              </div>
            </div>
            <div class="col">
              <div class="card">
                <div class="card-header">
                  آخرین کاربران
                </div>
                <div class="card-body">
                  <!-- //later we add a table here -->
                  
                  <?php 
                  for ($i=0; $i < sizeof($data); $i++) {
                    echo "<span class='badge badge-secondary'>".($data[3][$i]['user_name'])."</span>";
                    echo " ";
                  }
                  ?>
                </div>
              </div>
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col">
              <div class="card">
                <div class="card-header">
                  تعداد پست ها و کاربران
                </div>
                <div class="card-body">
                    <?= $data[0]?> پست و
                      <?= $data[1]?> کاربر
                </div>
              </div>
            </div>
            <div class="col">
              <div class="card">
                <div class="card-header">
                  آمار بازدید از سایت
                </div>
                <div class="card-body">

                </div>
              </div>
            </div>

          </div>
        </div>
      </div>
    </div>
  </div>

  </div>


</div>
<script type="text/javascript">
function openNav() {
  document.getElementById("mySidenav").style.width = "250px";
}
function closeNav() {
  document.getElementById("mySidenav").style.width = "0";
}
</script>
<?php } ?>

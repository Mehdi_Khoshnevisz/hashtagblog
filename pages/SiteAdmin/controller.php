<?php
session_start();
function validateInputs($username,$password){
  //validate
  $valid = true;
  return $valid;
}
function authenticationUser(){
  if(isset($_SESSION['user'])){
    if($_SESSION['user']=== "Administrator")
      return viewPanel();
    else
      header( "Location:  http://127.0.0.1/hashtagblog" );
  }elseif ($_SERVER['REQUEST_METHOD'] === 'GET') {
    return viewLogin();
  }elseif($_SERVER['REQUEST_METHOD'] === 'POST'){
    $valid = validateInputs($_POST['username'] , $_POST['password']);
    if($valid){
      //search in db to authenticationUser
      $found = model_Auth_Admin($_POST['username'] , $_POST['password']);
      if($found){
        $_SESSION['user']= "Administrator";
        return viewPanel();
      }
    }
  }
}
function getPanelInfo(){
  if(isset($_SESSION['user']))
    if ($_SESSION['user']==="Administrator") {
      $numberOfPosts = model_getStatistics("posts");
      $numberOfUsers = model_getStatistics("users");
      $lastPosts = model_getLasts(5 , "posts");
      $lastUsers = model_getLasts(5 , "users");
      return array($numberOfPosts, $numberOfUsers, $lastPosts ,$lastUsers);
    }
}
 ?>

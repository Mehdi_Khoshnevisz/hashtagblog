<?php 
session_start();
if(isset($_SESSION['user'])){      // if there is no valid session
    header("Location: http://127.0.0.1/hashtagblog");
}
else{
    include './scripts/server/connect_db.php';
    if(isset($_POST['email']) && isset($_POST['password'])){
        $email    = $_REQUEST['email'];
        $password = $_REQUEST['password'];
        $get_user = "SELECT * FROM users";
        $result   = mysqli_query($connect,$get_user);
    
        if ( $result ) echo 'got it<br>';
        if ( mysqli_num_rows($result) > 0 ) {
            // output data of each row
            while($row = mysqli_fetch_assoc($result)) {
                if($row['email'] == $email && $row['password'] == $password){
                    $_SESSION['name'] = $row['name'];
                    $_SESSION['user'] = $row['user_name'];
                    $_SESSION['user_id'] = $row['id'];
                    header( "Location:  http://127.0.0.1/hashtagblog" );
                    // echo "<div style='direction:ltr'>id: " . $row["id"]. " - Name: " . $row["name"]. " - Email: " . $row["email"]. "😀</div><br>";
                }else{ 
                    header( "Location:  http://127.0.0.1/hashtagblog/signin" );
                    // echo "<div style='direction:ltr'>id: " . $row["id"]. " - Name: " . $row["name"]. " - Email: " . $row["email"]. "</div><br>";
                }  
            }
        }
    }
}


?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>ورود</title>
    <link rel="shortcut icon" href="./styles/images/Hashtag-Blog-Logo.ico" type="image/x-icon">
    <link rel="stylesheet" type="text/css" media="screen" href="./node_modules/bootstrap/dist/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="./styles/css/main.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="./styles/css/header.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="./styles/css/footer.css" />
</head>
<body>
    <form action="" method="post" name="signin_form" class="d-flex flex-column justify-content-center align-items-center" style="height:100vh">
        <div class="text-center flex-1">
            <div class="d-flex justify-content-between align-items-center py-3">
                <h3 class="text-secondary m-0">ورود به</h3>
                <a href="/hashtagblog"><img src="styles/images/Hashtag-Blog-Logo.png" width="80"></a>
            </div>
            <div class="card box-shadow bg-light">
                <div class="form-group px-3 m-0">
                    <div class="my-3">
                      <input class="form-control" type="email" name="email" placeholder="ایمیل"/>
                    </div>
                    <div class="my-3">
                        <input class="form-control" type="password" name="password" placeholder="رمز عبور" />
                    </div>
                    <div class="my-3">
                        <button class="btn btn-sm w-100 btn-success rounded">ورود</button>
                    </div>
                </div>
            </div>
            <a href="signup" class="d-block mt-3">هنوز ثبت نام نکرده اید ؟ 😳</a>
        </div>
    </form>
</body>
</html>
<?php
// write a post
session_start();
include './scripts/server/connect_db.php';

if(isset($_SESSION['user'])){
  $selectB = "SELECT id,name from  `blogs` WHERE user_id = '".$_SESSION['user_id']."'";
  $result2   = mysqli_query($connect,$selectB);
    if(isset($_POST['postTitle']) && isset($_POST['text'])&& isset($_POST['selectBlog'])){
      $postTitle  = $_REQUEST['postTitle'];
      $text = $_REQUEST['text'];
      $selectBlog = $_REQUEST['selectBlog'];
      $q = "INSERT INTO `posts` (`blog_id`, `title`, `text`) VALUES (".$selectBlog." , '".$postTitle."' , '".$text."')";
      $result   = mysqli_query($connect,$q);
      if($result)
        header( "Location:  http://127.0.0.1/hashtagblog/successful" );
      else {
        mysqli_errno($connect);
      }
    }
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8" />
    <title> ایجاد پست جدید </title>
    <link rel="shortcut icon" href="./styles/images/Hashtag-Blog-Logo.ico" type="image/x-icon">
    <link rel="stylesheet" type="text/css" media="screen" href="./node_modules/bootstrap/dist/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="./styles/css/base.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="./styles/css/main.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="./styles/css/header.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="./styles/css/footer.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="./styles/css/search.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="./styles/css/blogs.css" />
  </head>
  <body>
    <div class="main">
      <?php include './partials/header.php' ?>

      <?php include './partials/search.php' ?>
      <br>
      <div class="mx-auto card text-right" style="width: 50rem;">
        <div class="card-header">
          اضافه کردن پست جدید
        </div>
        <div class="card-body">
          <form action="" method="post" name="blog-write" >
            <div class="input-group mb-3">
              <input type="text" name="postTitle" class="form-control" placeholder="عنوان نوشته" aria-label="postTitle" aria-describedby="basic-addon1" required >
            </div>
            <div class="input-group mb-3 text-left" style="direction:ltr;">
              <div class="input-group-prepend">
                <label class="input-group-text" for="selectBlog">انتخاب بلاگ</label>
              </div>
              <select class="custom-select font-size-9" name="selectBlog" id="selectBlog" required>
                <?php while ($rows = mysqli_fetch_assoc($result2)) {
                  ?>
                  <option value=<?=$rows['id']?> > <?=$rows['name']?> </option>
                  <?php
                } ?>
              </select>
            </div>
            <div class="form-group">
              <textarea class="form-control" placeholder="متن" name="text" id="exampleFormControlTextarea1" rows="5" required></textarea>
            </div>
            <button class="btn btn-sm px-5 btn-primary rounded">اضافه کردن </button>
          </form>
        </div>
      </div>
      <br>
      <?php include './partials/footer.php' ?>


    </div>

  </body>
</html>
<?php
}else{
  header( "Location:  http://127.0.0.1/hashtagblog/" );
}
?>

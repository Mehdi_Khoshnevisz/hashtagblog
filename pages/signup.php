<?php
session_start();
if(isset($_SESSION['user'])){      // if there is no valid session
    header("Location: http://127.0.0.1/hashtagblog");
}
else{
    include './scripts/server/connect_db.php';
    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        $user_name = $_REQUEST['user_name'];
        $name      = $_REQUEST['name'];
        $email     = $_REQUEST['email'];
        $password  = $_REQUEST['password'];
        $sql       = "INSERT INTO users (user_name,name,email,password,role) VALUES ('$user_name','$name','$email','$password','normal')";
      
        // var_dump(mysqli_fetch_assoc($user_array));
        
        // $_SESSION['user_id'] = $sql_users_result + 1;
        
        if ( mysqli_query($connect,$sql) ) {
            $_SESSION['user'] = $user_name;
            $_SESSION['name'] = $name;
            $sql_users = "SELECT id FROM users WHERE user_name = '$user_name'";
            $sql_users_result = mysqli_query($connect,$sql_users);
            $fetch_row = mysqli_fetch_assoc($sql_users_result);
            $_SESSION['user_id'] = (int)$fetch_row['id'];
            header("Location: http://127.0.0.1/hashtagblog");
        }

        else echo "Error: " . $sql . "<br>" . mysqli_error($connect);
    }
}

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>ثبت نام</title>
    <link rel="shortcut icon" href="./styles/images/Hashtag-Blog-Logo.ico" type="image/x-icon">
    <link rel="stylesheet" type="text/css" media="screen" href="./node_modules/bootstrap/dist/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="./styles/css/main.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="./styles/css/header.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="./styles/css/footer.css" />
</head>
<body>
    <form action="#" method="post" class="d-flex flex-column justify-content-center align-items-center" style="height:100vh">
        <div class="text-center flex-1">
            <div class="d-flex justify-content-between align-items-center py-3">
                <h3 class="text-secondary m-0">ثبت نام در</h3>
                <a href="/hashtagblog"><img src="styles/images/Hashtag-Blog-Logo.png" width="80"></a>
            </div>
            <div class="card box-shadow bg-light">
                <div class="form-group px-3 m-0">
                    <div class="my-3">
                        <input class="form-control" type="text" name="user_name" placeholder="نام کاربری"/>
                    </div>
                    <div class="my-3">
                        <input class="form-control" type="text" name="name" placeholder="نام و نام خانوادگی"/>
                    </div>
                    <div class="my-3">
                        <input class="form-control" type="email" name="email" placeholder="ایمیل" />
                    </div>
                    <div class="my-3">
                        <input class="form-control" type="password" name="password" placeholder="رمز عبور" />
                    </div>
                    <div class="my-3">
                        <button class="btn btn-sm w-100 btn-success rounded">ثبت نام</button>
                    </div>
                </div>
            </div>
            <a href="signin" class="d-block mt-3">وارد شوید...</a>
        </div>
    </form>
</body>
</html>

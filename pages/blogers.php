<?php
session_start();
include './scripts/server/connect_db.php';
$sql = "SELECT name,id from users";
$result = mysqli_query($connect,$sql);
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>بلاگر ها</title>
    <link rel="shortcut icon" href="./styles/images/Hashtag-Blog-Logo.ico" type="image/x-icon">
    <link rel="stylesheet" type="text/css" media="screen" href="./node_modules/bootstrap/dist/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="./styles/css/main.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="./styles/css/base.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="./styles/css/header.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="./styles/css/footer.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="./styles/css/blogs.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="./styles/css/search.css" />
</head>
</head>
<body>
    <main>

        <?php include './partials/header.php' ?>

        <?php include './partials/search.php' ?>

        <div style="height:500px;background-image:url(./styles/images/develoeprbg.jpg);    
        background-size: cover;
        background-position: center;)" class=""></div>

        <section class="container py-3">
            <div class="d-flex">
                <div class="col-3 bg-secondary p-2 text-right">
                    <h3 class="d-inline-block font-size-9 text-right">تعداد کاربران: </h3>
                    <span class="d-inline-block font-size-9"><?php echo mysqli_num_rows($result) ?></span>
                </div>
                <div class="col-9">

                    <div class="d-flex flex-wrap align-items-center">

                        <?php if(mysqli_num_rows($result) > 0){
                            while($row = mysqli_fetch_assoc($result)){
                        ?>
                            <div class="p-0">
                                <div class="border text-right bg-light px-3 py-1 ml-2 mb-2">
                                    <a class="font-size-9" href="./user?id=<?=$row['id']?>&user=<?=$row['name']?>">
                                        <figure class="d-flex align-items-center m-0  rounded">
                                            <img class="align-middle" src="./styles/images/noavatar.png" width="40" height="40">
                                            <figcaption class="mr-2 text-secondary"><?=$row['name']?></figcaption>
                                        </figure>
                                    </a>
                                </div>
                            </div>
                        <?php }
                        }?>
                    </div>

                </div>
            </div>
        </section>

    </main>

    <!-- scripts -->
    <script src="node_modules/jquery/dist/jquery.min.js"></script>
    <script src="scripts/client/public.js"></script>
    <script src="scripts/client/search.js"></script>
</body>
</html>

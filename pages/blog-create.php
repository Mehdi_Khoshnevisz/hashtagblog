<?php
    session_start();
    session_regenerate_id();
    if(!isset($_SESSION['user'])){      // if there is no valid session
        header("Location: http://127.0.0.1/hashtagblog/signin");
    }
    else {
        include './scripts/server/connect_db.php';
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            $blog_name = $_REQUEST['blog_name'];
            $user_id = $_SESSION['user_id'];
            $about_blog = $_REQUEST['about_blog'];
            $category = $_REQUEST['category'];
            $blog_cover = $_REQUEST['blog_cover'];
            $sql = "INSERT INTO blogs (name,user_id,caption,category,cover) VALUES ('$blog_name','$user_id','$about_blog','$category','$blog_cover')";
            if ( mysqli_query($connect,$sql) ) header('Location: http://127.0.0.1/hashtagblog/successful');
            else echo "Error: " . $sql . "<br>" . mysqli_error($connect);
        }
    }

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>ساخت بلاگ</title>
    <link rel="shortcut icon" href="./styles/images/Hashtag-Blog-Logo.ico" type="image/x-icon">
    <link rel="stylesheet" type="text/css" media="screen" href="./node_modules/bootstrap/dist/css/bootstrap.min.css" />
    <!-- <link rel="stylesheet" type="text/css" media="screen" href="lib/fontawesome/fontawesome.css" /> -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" media="screen" href="./styles/css/main.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="./styles/css/header.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="./styles/css/footer.css" />
</head>
<body>
    <form action="" method="post" name="signin_form" class="d-flex flex-column justify-content-center align-items-center" style="height:100vh" >
        <div class="text-center flex-1">

            <div class="pt-3">
                <a href="/hashtagblog"><img src="styles/images/Hashtag-Blog-Logo.png" width="80"></a>
                <h3 class="text-secondary py-3 m-0" id="statusText">بلاگ خودت رو بساز...</h3>
            </div>
            <section class="step-parent">
                <!-- blog name -->
                <div class="step-item card border-0 p-3 my-3">
                    <span class="h6 font-weight-bold text-secondary mb-2 text-center bg-light py-2 px-3 shadow-sm">
                        <i class="fa fa-angle-left align-middle"></i> نام بلاگ خود را وارد کنید:
                    </span>
                    <input class="form-control form-control-sm" type="text" name="blog_name">
                    <button type="button" class="next-step btn btn-info btn-sm d-inline-block mt-3 w-50 mr-auto">برو بعدی</button>
                </div>
                <!-- about blog -->
                <div class="step-item card border-0 p-3 my-3">
                    <span class="h6 font-weight-bold text-secondary mb-2 text-center bg-light py-2 px-3 shadow-sm">
                        <i class="fa fa-angle-left align-middle"></i> در مورد بلاگ خود بگویید:
                    </span>
                    <textarea class="form-control form-control-sm" name="about_blog" id="" cols="30" rows="3"></textarea>
                    <button type="button" class="next-step btn btn-info btn-sm d-inline-block mt-3 w-50 mr-auto">برو بعدی</button>
                </div>
                <!-- category -->
                <div class="step-item card border-0 p-3 my-3">
                    <span class="h6 font-weight-bold text-secondary mb-2 text-center bg-light py-2 px-3 shadow-sm">
                        <i class="fa fa-angle-left align-middle"></i> دسته بندی بلاگ شما چیست ؟
                    </span>
                    <select class="form-control form-control-sm" name="category" id="">
                        <option value="programming">برنامه نویسی</option>
                        <option value="network">شبکه</option>
                        <option value="startup">استارتاپ</option>
                    </select>
                    <button type="button" class="next-step btn btn-info btn-sm d-inline-block mt-3 w-50 mr-auto">برو بعدی</button>
                </div>
                <!-- cover -->
                <div class="step-item card border-0 p-3 my-3">
                    <span class="h6 font-weight-bold text-secondary mb-2 text-center bg-light py-2 px-3 shadow-sm">
                        <i class="fa fa-angle-left align-middle"></i> کاور بلاگ خود را انتخاب کنید:
                    </span>
                    <input type="file" id="choose-file" name="blog_cover" class="form-control form-control-sm d-none">
                    <div class="d-flex align-items-center">
                        <button type="button" class="coverButton btn btn-primary btn-sm d-inline-block w-50 ">انتخاب کاور</button>
                        <span class="coverName w-50 text-secondary pr-3">نام کاور...</span>
                    </div>
                    <button type="submit" class="next-step btn btn-info btn-sm d-inline-block mt-3 w-50 mr-auto">تمومش کن</button>
                </div>
            </section>

        </div>
    </form>
    <!-- scripts -->
    <script src="node_modules/jquery/dist/jquery.min.js"></script>
    <script src="scripts/client/public.js"></script>
    <script src="scripts/client/steps.js"></script>
    <script src="scripts/client/choose-file.js"></script>
</body>
</html>

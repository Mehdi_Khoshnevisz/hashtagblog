<?php
session_start();
include './scripts/server/connect_db.php';
if(isset($_GET['id'])){
  $sql_blogs = "SELECT * from blogs WHERE user_id = '".$_GET['id']."'";
  $sql_user = "SELECT id,user_name,email,name from users WHERE id = '".$_GET['id']."'";

  $result = mysqli_query($connect,$sql_blogs);
  $result_user = mysqli_query($connect,$sql_user);
  $result_user = mysqli_fetch_assoc($result_user);
  ?>

  <!DOCTYPE html>
  <html>
  <head>
      <meta charset="utf-8" />
      <title>پنل کاربر</title>
      <link rel="shortcut icon" href="./styles/images/Hashtag-Blog-Logo.ico" type="image/x-icon">
      <link rel="stylesheet" type="text/css" media="screen" href="./node_modules/bootstrap/dist/css/bootstrap.min.css" />
      <link rel="stylesheet" type="text/css" media="screen" href="./styles/css/main.css" />
      <link rel="stylesheet" type="text/css" media="screen" href="./styles/css/base.css" />
      <link rel="stylesheet" type="text/css" media="screen" href="./styles/css/header.css" />
      <link rel="stylesheet" type="text/css" media="screen" href="./styles/css/footer.css" />
      <link rel="stylesheet" type="text/css" media="screen" href="./styles/css/search.css" />
      <link rel="stylesheet" type="text/css" media="screen" href="./styles/css/blogs.css" />
  </head>
  </head>
  <body>

      <main>

          <?php include './partials/header.php' ?>

          <?php include './partials/search.php' ?>

          <div style="height:300px" class="bg-dark position-relative">
              <div class="position-absolute" style="bottom:-42px;right: 118px;">
                  <img class="rounded-circle shadow-sm border" src="./styles/images/noavatar.png" width="90" height="90" alt="">
              </div>
          </div>
          <br>
          <section>
            <div class="d-flex">
                <div class="col-3 text-center pt-5">
                    <span class="d-inline-block text-secondary mr-1 px-3"><?php echo $result_user['name']?></span><br>
                    <?php
                      if(@$_SESSION['user_id']!=$result_user['id'])
                        echo "<button class=' mx-2 btn btn-sm py-1 px-3 mt-1 btn-success'>دنبال کردن</button>";
                      else
                        echo "<button class=' mx-2 btn btn-sm py-1 px-3 mt-1 btn-success'>ویرایش پروفایل</button>";
                    ?>
                    <button class=' mx-2 btn btn-sm py-1 px-3 mt-1 btn-light'> 123 دنبال کننده </button>
                    <hr/>
                    <div class="card">
                      <div class="card-body">
                      <h5 class="card-title"> اطلاعات کاربر </h5>
                      <h6 class="card-subtitle mb-2 text-muted text-right"> ادرس ایمیل : <?= $result_user['email'] ?></h6>
                      <h6 class="card-subtitle mb-2 text-muted text-right"> نام کاربری : <?= $result_user['user_name'] ?></h6>
                    </div>

                  </div><br>
                </div>
                <div class="col-9">
                  <div class="container d-flex flex-wrap">
                  <?php 
                  if(mysqli_num_rows($result) > 0){
                  while($row = mysqli_fetch_assoc($result)){
                      ?>
                    <div class="col-md-4 px-2 my-2">
                        <div class="card blogItem bg-light text-right p-3">
                            <figure class="m-0">
                                <a href="./posts?blogId=<?=$row['id']?>">
                                    <img class="w-100 mb-3 rounded" src="styles/images/blog_img.jpg" >
                                </a>
                                <figcaption class="m-0">
                                    <h3 class="font-size1 font-weight-bold m-0"><?=$row['name']?></h3>
                                </figcaption>
                            </figure>
                        </div>
                    </div>
                  <?php }} else echo 'هیچ بلاگی وجود ندارد!' ?>
                  </div>
                </div>
            </div>
          </section>

      </main>
      <?php include './partials/footer.php' ?>
      <!-- scripts -->
      <script src="node_modules/jquery/dist/jquery.min.js"></script>
      <script src="scripts/client/public.js"></script>
      <script src="scripts/client/search.js"></script>
  </body>
  </html>
  <?php
  }else {
    header( "Location:  http://127.0.0.1/hashtagblog/" );
  } ?>

<?php
session_start();
include './scripts/server/connect_db.php';
$sql = "SELECT blogs.name,blogs.id,blogs.user_id,blogs.caption ,users.user_name FROM blogs INNER JOIN users ON users.id = blogs.user_id";
$result = mysqli_query($connect,$sql);

?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>بلاگ ها</title>
    <link rel="shortcut icon" href="./styles/images/Hashtag-Blog-Logo.ico" type="image/x-icon">
    <link rel="stylesheet" type="text/css" media="screen" href="./node_modules/bootstrap/dist/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="./styles/css/base.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="./styles/css/main.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="./styles/css/header.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="./styles/css/footer.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="./styles/css/search.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="./styles/css/blogs.css" />
</head>
</head>
<body>

    <main>

        <?php include './partials/header.php' ?>

        <?php include './partials/search.php' ?>

        <div style="background-image:url('./styles/images/blog-cover.jpg')"
            class="cover-background bg-dark border-bottom"></div>

        <section class="container py-3 border-bottom">
            <div class="col-12 text-right py-3">
                <h4 class="m-0 font-weight-bold">برنامه نویسی</h4>
            </div>
            <div class="d-flex flex-wrap">
                <?php while($row = mysqli_fetch_assoc($result)){?>
                <div class="col-md-4 px-2 my-2">
                    <div class="card blogItem bg-light text-right p-3">
                        <figure class="m-0">
                            <a href="./posts?blogId=<?=$row['id']?>">
                                <img class="w-100 mb-3 rounded" src="styles/images/blog_img.jpg" >
                            </a>
                            <figcaption class="m-0">
                                <h3 class="font-size1-1 font-weight-bold m-0"><?php echo $row['name']?></h3>
                                <p class="p-0 m-0 mt-1 textcut text-justify font-size-9 text-secondary">
                                <!-- لورم اپیسوم متن ساختگی است برای نمایش دادن متون تستی در وبسایت ها و وبلاگ ها -->
                                <?php echo $row['caption']?>
                                </p>
                                <div class="d-flex align-items-center mt-1">
                                    <svg width="20" height="20" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                        viewBox="0 0 53 53" style="enable-background:new 0 0 53 53;" xml:space="preserve">
                                        <path style="fill:#E7ECED;" d="M18.613,41.552l-7.907,4.313c-0.464,0.253-0.881,0.564-1.269,0.903C14.047,50.655,19.998,53,26.5,53
                                            c6.454,0,12.367-2.31,16.964-6.144c-0.424-0.358-0.884-0.68-1.394-0.934l-8.467-4.233c-1.094-0.547-1.785-1.665-1.785-2.888v-3.322
                                            c0.238-0.271,0.51-0.619,0.801-1.03c1.154-1.63,2.027-3.423,2.632-5.304c1.086-0.335,1.886-1.338,1.886-2.53v-3.546
                                            c0-0.78-0.347-1.477-0.886-1.965v-5.126c0,0,1.053-7.977-9.75-7.977s-9.75,7.977-9.75,7.977v5.126
                                            c-0.54,0.488-0.886,1.185-0.886,1.965v3.546c0,0.934,0.491,1.756,1.226,2.231c0.886,3.857,3.206,6.633,3.206,6.633v3.24
                                            C20.296,39.899,19.65,40.986,18.613,41.552z"/>
                                        <path style="fill:#556080;" d="M26.953,0.004C12.32-0.246,0.254,11.414,0.004,26.047C-0.138,34.344,3.56,41.801,9.448,46.76
                                            c0.385-0.336,0.798-0.644,1.257-0.894l7.907-4.313c1.037-0.566,1.683-1.653,1.683-2.835v-3.24c0,0-2.321-2.776-3.206-6.633
                                            c-0.734-0.475-1.226-1.296-1.226-2.231v-3.546c0-0.78,0.347-1.477,0.886-1.965v-5.126c0,0-1.053-7.977,9.75-7.977
                                            s9.75,7.977,9.75,7.977v5.126c0.54,0.488,0.886,1.185,0.886,1.965v3.546c0,1.192-0.8,2.195-1.886,2.53
                                            c-0.605,1.881-1.478,3.674-2.632,5.304c-0.291,0.411-0.563,0.759-0.801,1.03V38.8c0,1.223,0.691,2.342,1.785,2.888l8.467,4.233
                                            c0.508,0.254,0.967,0.575,1.39,0.932c5.71-4.762,9.399-11.882,9.536-19.9C53.246,12.32,41.587,0.254,26.953,0.004z"/>
                                    </svg>
                                    <a href="user?id=<?php echo $row['user_id']?>&user=<?php echo $row['user_name']?>" class="mr-1 font-size-8 text-gray-500"><?php echo $row['user_name'] ?></a>
                                </div>
                            </figcaption>
                        </figure>
                    </div>
                </div>
                <?php }?>
            </div>
        </section>

        <?php include './partials/footer.php' ?>

    </main>

    <!-- scripts -->
    <script src="node_modules/jquery/dist/jquery.min.js"></script>
    <script src="scripts/client/public.js"></script>
    <script src="scripts/client/search.js"></script>
</body>
</html>

<?php
session_start();
include './scripts/server/connect_db.php';
if(isset($_GET['blogId'])){
  $sql = "SELECT * from posts WHERE blog_id='".$_GET['blogId']."'";
  $result = mysqli_query($connect,$sql);
  $q_resultBlogName = "SELECT * from blogs WHERE id='".$_GET['blogId']."'";
  $resultBlogName = mysqli_fetch_assoc(mysqli_query($connect,$q_resultBlogName));
  $resultBlogCaption = $resultBlogName['caption'];
  $resultBlogName = $resultBlogName['name'];
  ?>

  <!DOCTYPE html>
  <html>
  <head>
      <meta charset="utf-8" />
      <title>بلاگ ها</title>
      <link rel="shortcut icon" href="./styles/images/Hashtag-Blog-Logo.ico" type="image/x-icon">
      <link rel="stylesheet" type="text/css" media="screen" href="./node_modules/bootstrap/dist/css/bootstrap.min.css" />
      <link rel="stylesheet" type="text/css" media="screen" href="./styles/css/base.css" />
      <link rel="stylesheet" type="text/css" media="screen" href="./styles/css/main.css" />
      <link rel="stylesheet" type="text/css" media="screen" href="./styles/css/header.css" />
      <link rel="stylesheet" type="text/css" media="screen" href="./styles/css/footer.css" />
      <link rel="stylesheet" type="text/css" media="screen" href="./styles/css/search.css" />
      <link rel="stylesheet" type="text/css" media="screen" href="./styles/css/blogs.css" />
  </head>
  </head>
  <body>

      <main>

          <?php include './partials/header.php' ?>

          <?php include './partials/search.php' ?>

          <div style="background-image:url('./styles/images/blog-cover.jpg')"
              class="cover-background bg-dark border-bottom"></div>

          <section class="container py-3 border-bottom">
              <div class="col-12 text-right px-0 py-3">
                  <h4 class="m-0 text-dark font-size1-2">پست های بلاگ <span class="font-size1-2 font-weight-bold text-dark"><?=$resultBlogName?></span></h4>
              </div>
              <div>
                <p class="font-size-8 text-gray-600 text-right">
                  <?= $resultBlogCaption ?>
                </p>
              </div>
              <div class="col-12 px-2 my-2 bg-light">
              <div class="card blogItem bg-light text-right p-3">
              <div class="card">
                <ul class="list-group px-2 list-group-flush">
                  <?php
                  if(mysqli_num_rows($result) > 0){
                    while($row = mysqli_fetch_assoc($result)){
                    ?>
                    <li class="list-group-item font-size-8">
                      <a href="post?post_id=<?php echo $row['id']?>&blog_id=<?php echo $row['blog_id']?>" > <?=$row['title']?> </a>
                    </li>
                    <?php
                    }
                  }else{ ?>
                    <span class="font-size-8 my-2 text-gray-600 text-right">
                    هنوز پستی ثبت نشده است
                    </span>
                  <?php
                  }
                  ?>
                </ul>
              </div>
            </div>
              </div>
          </section>

          <?php include './partials/footer.php' ?>

      </main>

      <!-- scripts -->
      <script src="node_modules/jquery/dist/jquery.min.js"></script>
      <script src="scripts/client/public.js"></script>
      <script src="scripts/client/search.js"></script>
  </body>
  </html>
<?php
}else {
  header( "Location:  http://127.0.0.1/hashtagblog/" );
} ?>

<?php
session_start();
include './scripts/server/connect_db.php';
if(isset($_GET['post_id']) && isset($_GET['blog_id'])){
  $sql_posts = "SELECT * from posts WHERE id = '".$_GET['post_id']."' AND blog_id = '".$_GET['blog_id']."' ";
  $result = mysqli_query($connect,$sql_posts);
  $post = mysqli_fetch_assoc($result);
}
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title><?php echo $post['title']?></title>
    <link rel="shortcut icon" href="./styles/images/Hashtag-Blog-Logo.ico" type="image/x-icon">
    <link rel="stylesheet" type="text/css" media="screen" href="./node_modules/bootstrap/dist/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="./styles/css/base.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="./styles/css/main.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="./styles/css/header.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="./styles/css/footer.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="./styles/css/search.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="./styles/css/blogs.css" />
</head>
</head>
<body>

    <main>

        <?php include './partials/header.php' ?>

        <?php include './partials/search.php' ?>

        <div style="height:300px" class="bg-secondary d-flex align-items-center justify-content-center">
            <span><?php echo $post['title']?></span>
        </div>

        <section class="container py-3 border-bottom">
            <div class="col-12 text-right py-3">
                <h4 class="m-0 font-weight-bold"></h4>
            </div>
            <div class="font-size1 text-gray-800 text-right mb-3">
                <?php echo $post['text']?>
            </div>
        </section>

        <?php include './partials/footer.php' ?>

    </main>

    <!-- scripts -->
    <script src="node_modules/jquery/dist/jquery.min.js"></script>
    <script src="scripts/client/public.js"></script>
    <script src="scripts/client/search.js"></script>
</body>
</html>

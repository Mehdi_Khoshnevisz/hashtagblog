<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>ساخت بلاگ</title>
    <link rel="shortcut icon" href="./styles/images/Hashtag-Blog-Logo.ico" type="image/x-icon">
    <link rel="stylesheet" type="text/css" media="screen" href="./node_modules/bootstrap/dist/css/bootstrap.min.css" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" media="screen" href="./styles/css/main.css" />
</head>
<body>

    <section class="d-flex flex-column justify-content-center align-items-center" style="height:100vh" >
        <div class="text-center flex-1">
          <h3 class="text-dark">تبریک ! بلاگ شما باموفقیت ساخته شد 😃👏</h3>
          <a href="./">
            <button type="button" name="button" class="btn btn-primary">بازگشت</button>
          </a>
        </div>
    </section>
 btn-primary
</body>
</html>
